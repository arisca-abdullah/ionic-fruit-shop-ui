import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PassDataService } from '../pass-data.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
})
export class DetailsPage implements OnInit {

  amount = 0;
  showHeader = false;
  data: any;

  constructor(private router: Router, private passData: PassDataService) {
    if(!this.passData.getParams()) {
      this.router.navigate(['home']);
    } else {
      this.data = this.passData.getParams();
    }
  }

  ngOnInit() {
    
  }

  addAmount() {
    this.amount = this.amount + 1;
    console.log(this.amount);
  }

  minAmount() {
    if(this.amount > 0) {
      this.amount = this.amount - 1;
    }
    console.log(this.amount);
  }

  toggleHeader(e) {
    if(e.detail.scrollTop > 0) {
      if(!this.showHeader) this.showHeader = true;
    } else {
      if(this.showHeader) this.showHeader = false;
    }
  }

}
