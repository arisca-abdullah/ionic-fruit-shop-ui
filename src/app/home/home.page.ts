import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { PassDataService } from '../pass-data.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  showHeader = false;
  fruits = [
    {
      name: "orange",
      "bg-color": "#ECE081",
      cost: "3000 IDR"
    },
    {
      name: "apple",
      "bg-color": "#C6F390",
      cost: "4000 IDR"
    },
    {
      name: "kiwi",
      "bg-color": "#C5FF68",
      cost: "3500 IDR"
    },
    {
      name: "lychee",
      "bg-color": "#FFB0A7",
      cost: "1000 IDR"
    },
    {
      name: "banana",
      "bg-color": "#F2FF82",
      cost: "1500 IDR"
    },
    {
      name: "guava",
      "bg-color": "#FFB394",
      cost: "5000 IDR"
    }
  ]

  constructor(private router:Router, private passData: PassDataService) {}

  toggleHeader(e) {
    if(e.detail.scrollTop > 0) {
      if(!this.showHeader) this.showHeader = true;
    } else {
      if(this.showHeader) this.showHeader = false;
    }
  }

  async seeDetails(data) {
    this.passData.setParams(data);
    if(this.passData.getParams()) {
      await this.router.navigate(['/details']);
    }
  }

}
