import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PassDataService {
  private params: any;

  setParams(data) {
    this.params = data;
  }

  getParams() {
    if (!this.params) {
      return undefined;
    }
    return this.params;
  }
}
